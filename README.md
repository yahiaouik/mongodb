## Projet scolaire Equipes de foot

### Introduction
Ce projet a pour but de prendre en main la technologie MongoDb.
L'objectif est de représenter des equipes, des joueurs, des matchs et les différentes relations qui les relient.
Ce projet est a été réalisé en JavaScript grâce à un serveur Nodejs.

### Étapes pour utiliser le code

#### prérequis

Avoir Nodejs et Mongodb d'installer

#### Étapes

Etape 1 : Créer une base de donnée locale qui s'appelle nosql et la lancer. ( la bdd doit tourner sur le port 27017)
Etape 2 : Récupérer le code et installer les modules nécessaires : npm install
Etape 3 : Pour lancer le code : npm run start

#### Résultats
Les résultats des différentes fonctions sont visibles dans la console.
Pour une représentation graphique, utiliser MongoDB Compass.
