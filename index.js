import {clearDB, init} from './db_connection.js'
import equipeManager from './src/EquipeManager.js'
import joueurManager from './src/JoueurManager.js'
import matchManager from './src/MatchManager.js'

await clearDB();
await init();

// Creations de joueurs 
const _id_joueur_1= await joueurManager.insertJoueur("Raffier", "Stephane", "1986-09-27", "1.88","90","gardien");
const _id_joueur_2= await joueurManager.insertJoueur( "Fofana","Wesley", "2000-12-17", "1.9","80","defenseur");
const _id_joueur_3= await joueurManager.insertJoueur( " Trauco","Miguel", "1992-08-25", "1.7","62","defenseur");
const _id_joueur_4= await joueurManager.insertJoueur("Hamouma", "Romain", "1987-03-29", "1.78","74","attaquant");

const _id_joueur_5= await joueurManager.insertJoueur("Thauvin", "Florian", "1993-01-26", "1.79","70","milieu");
const _id_joueur_6= await joueurManager.insertJoueur("Henrique", "Luis", "2001-12-14", "1.82","78","ailier");
const _id_joueur_7= await joueurManager.insertJoueur("Benedetto", "Dario", "1990-05-17", "1.75","67","attaquant");
const _id_joueur_8= await joueurManager.insertJoueur("Ake", "Marley", "2001-01-05", "1.77","70","attaquant");

// Affiche tous les joueurs existant
const joueurs = await joueurManager.selectAllJoueur();
console.log("les joueurs sont : ");
console.log(joueurs);

// Affiche les joueurs qui ont moins de 25 ans et qui sont attaquant
const attaquants = await joueurManager.selectJoueurFromPosteAndAge("attaquant",25);
console.log("les joueurs sont : ");
console.log(attaquants);

// Creation de l'equipe asse et affichage des infos ajoutées
const _id_equipe_1 = await equipeManager.insertEquipe("asse","vert","stade geoffroy guichard ",[]);
const equipe_1 = await equipeManager.selectEquipeByName("asse")
console.log("Voici les informations de l'equipe : \n ");
console.log(equipe_1);

// Creation de l'equipe OM et affichage des infos ajoutées
const _id_equipe_2 = await equipeManager.insertEquipe("OM","bleu","stade Orange Velodrome ",[]);
const equipe_2 = await equipeManager.selectEquipeByName("OM")
console.log("Voici les informations de l'equipe : \n ");
console.log(equipe_2);

// Affectation des joueurs a leurs equipes
await equipeManager.addJoueur("asse", _id_joueur_1);
await equipeManager.addJoueur("asse", _id_joueur_2);
await equipeManager.addJoueur("asse", _id_joueur_3);
await equipeManager.addJoueur("asse", _id_joueur_4);
await equipeManager.addJoueur("OM", _id_joueur_5);
await equipeManager.addJoueur("OM", _id_joueur_6);
await equipeManager.addJoueur("OM", _id_joueur_7);
await equipeManager.addJoueur("OM", _id_joueur_8);

// Creation d'un match Asse Vs OM
const _id_match_1 = await matchManager.insertMatch(_id_equipe_1,_id_equipe_2,"coupe de france","3","2", [], []);

// Mise à jour des scores du match Asse Vs OM
await matchManager.updateScore(_id_match_1,"5","3");

// Mise à jour des notes des joueurs
await matchManager.updateNotes(_id_match_1, _id_equipe_1,true,{ _id_joueur : _id_joueur_1, note:  "1"},{ _id_joueur :_id_joueur_2, note : "3"},{ _id_joueur :_id_joueur_3, note : "4"},{ _id_joueur:_id_joueur_4, note : "0"})
await matchManager.updateNotes(_id_match_1, _id_equipe_2,false,{ _id_joueur : _id_joueur_5, note:  "1"},{ _id_joueur :_id_joueur_6, note : "3"},{ _id_joueur:_id_joueur_7, note : "4"},{ _id_joueur:_id_joueur_8, note : "0"})

// Creation d'une nouvelle collection qui contient le nom des joueurs  ayant au moins jouer 1 match avec leur moyenne
await matchManager.createNotesAverageCollection(1);