import { loadDB } from '../db_connection.js'
import joueurManger from './JoueurManager.js'

class MatchManager {

     // permet de creer un nouveau match
    async insertMatch(id_equipe_domicile, id_equipe_exterieur, competition, score_domicile, score_exterieur, notes_domicile, notes_exterieur) {
        const db = await loadDB();
        const result = await db.collection("matchs")
            .insertOne({ "equipe_domicile": id_equipe_domicile, "equipe_exterieur": id_equipe_exterieur, "competition": competition, "score_domicile": score_domicile, "score_exterieur": score_exterieur, "notes_domicile": notes_domicile, "notes_exterieur": notes_exterieur })
        return result.insertedId;
    }

    // permet de mettre a jour le score d'un match
    async updateScore(_id_match, score_domicile, score_exterieur) {
        const db = await loadDB();
        await db.collection("matchs").updateOne({ "_id": _id_match }, { $set: { "score_domicile": score_domicile } });
        await db.collection("matchs").updateOne({ "_id": _id_match }, { $set: { "score_exterieur": score_exterieur } });
    }

    // permet de mettre a jour les notes des joueurs d'un match
    async updateNotes(_id_match, _id_equipe, isDomicile, ...notes) {
        const db = await loadDB();
        if (isDomicile) {
            await db.collection("matchs").updateOne({ "_id": _id_match, "equipe_domicile": _id_equipe }, { $set: { "notes_domicile": notes } });
        } else {
            await db.collection("matchs").updateOne({ "_id": _id_match, "equipe_exterieur": _id_equipe }, { $set: { "notes_exterieur": notes } });
        }
    }
    
    // permet de creer une nouvelle collection qui contient le nom des joueurs ayant au moins jouer le nombre de matchs donnes avec leur moyenne
    async createNotesAverageCollection(nombreMatch) {

        const db = await loadDB();
        const joueurs = await joueurManger.selectAllJoueur()
        let matchs = [];
        for (let j = 0; j < joueurs.length; j++) {
            matchs = await db.collection("matchs").find({
                $or: [
                    { notes_domicile: { $elemMatch: { _id_joueur: joueurs[j]._id } } },
                    { notes_exterieur: { $elemMatch: { _id_joueur: joueurs[j]._id } } }
                ]
            },
                { projection: { notes_exterieur: { $elemMatch: { _id_joueur: joueurs[j]._id } }, notes_domicile: { $elemMatch: { _id_joueur: joueurs[j]._id } }, _id: 0 } }).toArray()

            
            let notes = []
            matchs.forEach(match => {
                notes.push(Object.values(match)[0][0].note);
            })

            //calcule la moyenne des notes
            const reducer = (accumulator, currentValue) => accumulator + currentValue;
            const avg = notes.reduce(reducer) / notes.length;

            // si le joueur a jouer au moins le nombre de match definie, on ajoute son id et sa moyenne a la nouvelle collection  
            if (notes.length >= nombreMatch) {
                await db.collection("nouvelleList").insertOne({ _id_joueur: joueurs[j]._id, avg });
            }
        }
    }

    // permet de rechercher tous les matchs
    async selectAllMatch(){
        const db = await loadDB()
        const result = await db.collection("match").find({}).toArray();
        return result;
    }

}

export default new MatchManager();
