import { loadDB } from '../db_connection.js'

class JoueurManager {

    // permet de creer un nouveau joueur
    async insertJoueur(nom, prenom, date_de_naissance, taille, poids, poste) {
        const db = await loadDB()
        const result = await db.collection("joueurs").insertOne({ "nom": nom, "prenom": prenom, "date_de_naissance": new Date(date_de_naissance), "taille": taille, "poids": poids, "poste": poste })
        return result.insertedId;
    }

    // permet de rechercher un joueur par nom
    async selectJoueurByName(nom, prenom) {
        const db = await loadDB()
        const result = await db.collection("joueurs").findOne(
            {
                nom: nom,
                prenom: prenom
            }
        );
        return result;
    }

    // permet de rechercher tous les joueurs existant
    async selectAllJoueur() {
        const db = await loadDB()
        const result = await db.collection("joueurs").find({}).toArray();
        return result;
    }

    //permet de rechercher les joueurs plus agé qu'un age donné qui ont un poste donné
    async selectJoueurFromPosteAndAge(poste, age) {
        const today = new Date();
        const date = new Date(today.setFullYear(today.getFullYear() - age));
        console.log(date);
        const db = await loadDB();
        const result = await db.collection("joueurs").find(
            {
                "poste": poste,
                "date_de_naissance": { $lt: date }
            }).toArray();

        return result;
    }


}


export default new JoueurManager();