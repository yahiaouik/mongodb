import {loadDB} from '../db_connection.js'

class EquipeManager {

    // permet de creer une nouvelle équipe
    async insertEquipe(nom,couleur,stade,effectif){
        const db = await loadDB();
        const result = await db.collection("equipes").insertOne({"nom": nom, "couleur" : couleur, "stade" : stade, "effectif":effectif});
        return result.insertedId;
    }

    // permet d'ajouter un joueur à une équipe
    async addJoueur(equipe, id_joueur){
        const db = await loadDB();
        await db.collection("equipes").updateOne({ "nom" : equipe}, {$push : {"effectif" : id_joueur }});
    }
    // permet de trouver une equipe depuis son nom
    async selectEquipeByName(nom) {
        const db = await loadDB()
        const result = await db.collection("equipes").findOne(
            {
               nom: nom
            }
         );
        return result;
    }
}

export default new EquipeManager();
