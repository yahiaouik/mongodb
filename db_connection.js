import mongodb from "mongodb" 
let db;

export async function loadDB() {
    if (db) {
        return db;
    }
    try {
        const client = await mongodb.connect(`mongodb://localhost:27017`,{ useUnifiedTopology: true });
        db = client.db('nosql');
    } catch (err) {
        Raven.captureException(err);
    }
    return db;
};

export async function clearDB() {
    const db = await loadDB();
    await db.collection("joueurs").deleteMany({})
    await db.collection("equipes").deleteMany({})
    await db.collection("matchs").deleteMany({})
    await db.collection("nouvelleList").deleteMany({})

}

export async function init(){
    // Optimisation des recherches
    const db = await loadDB();
    await db.collection("equipes").createIndex({nom: 1}, {nom: "equipeIndex"})
    await db.collection("joueurs").createIndex({prenom: 1, nom: 1}, {nom: "joueurIndex"})
}